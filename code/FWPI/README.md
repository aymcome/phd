# Floyd-Warshall Policy Iteration

This repository implements our new approach to Policy Evaluation, using a Floyd-Warshall like algorithm.

## Running an example

A showcase of the most interesting functionalities of the repository is available through the command

```
python3 example.py
```

In this example, a GridWorld maze is randomly generated as a case study; a near-optimal policy is computed for it, which is then evaluated by Value Iteration and Floyd-Warshall Policy Evaluation, with a graphical display of the value propagation through the iterations. Further comparison is done between the two algorithms by plotting the evolution of values with the number of operations done. Finally, a little batch experiment is launched for small sizes of GridWorld and RiverSwim, to evaluate the performances of the benchmark algorithms: FW, VI 5%, VI 1%, VI 0.1%, and Linear Solver.

### Structure and content

The implementation is object oriented, although legacy functions have been kept at the end of several scripts.

- **matrix.py** : re-implement matrices operations to benefit from `numpy`'s O(1) element access without taking advantage of the acceleration, for a fair comparison
- **MDP.py** : definition of the MDP class and RiverSwim and GridWorld subclasses
- **utility.py** : few handy functions for MDP manipulation/operation (Bellman operator, policy probability kernel), save MDPs information
- **floyd_warshall.py** : implementation of the Floyd-Warshall integration algorithm and functions to transform MDPs into weighted automaton
- **policy_evaluation.py** : implementation of the algorithms for (approximately) evaluating a policy on a MDP
- **learners.py** : implementation of the algorithms for finding an (near-)optimal policy
- **renderers.py** : visualisation tools for the algorithms
- **experiments.py** : run algorithms, and analyse the results with plots and statistics
- **environments.py** : functions to quickly generates some (random) MDPs
- **run_experiments.py** : launch experiments for performance comparison

## Versions and Dependencies

The package is written in Python 3.10.

It uses Numpy and matplotlib, please look into the [pyproject](./pyproject.toml) file for more details.