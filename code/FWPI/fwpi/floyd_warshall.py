"""
Floyd Warshall related functions to use on MDPs.
"""
import numpy as np


def to_weighted_graph_with_sink(mdp, gamma=0.99):
    """Transform the MDP into a weighted graph with a rewards sink state."""
    S, A = mdp.S, mdp.A

    graph = np.zeros((S+1, A, S+1))
    graph[:S, :, :S] = gamma * mdp.P  # discounted probabilities
    graph[:S, :, S] = mdp.R  # rewards as transitions towards sink state

    return graph


def to_policy_graph(graph, policy):
    """Restrict the graph to the transitions corresponding to the policy."""
    return graph[np.arange(graph.shape[0]), np.append(policy, 0), :]


def FW_step(W, s):
    """Returns the new W matrix after integrating s on W."""
    S = W.shape[0]

    w_to_s = W[:, s].copy()
    loop_on_s = 1/(1 - W[s, s])
    w_from_s = W[s, :].copy()
    for u in range(S):
        for v in range(S):
            W[u, v] += w_to_s[u] * loop_on_s * w_from_s[v]

    return W


def FW(W_0, order=None, inplace=True):
    """Integrate states (from order if given) on W_0 in FW fashion."""
    if order:
        iterator = order
    else:
        iterator = np.arange(W_0.shape[0])

    W = W_0 if inplace else W_0.copy()
    for s in iterator:
        W = FW_step(W, s)

    return W
