"""
Define various utility functions.
"""
import os
from os.path import join
import numpy as np
from matrix import Matrix, Tensor
from MDP import MDP, GridWorld


def load_MDP(dir_path):
    """Load the MDP saved in the directory path specified."""
    if os.path.exists(dir_path):
        P = np.load(join(dir_path, "P.npy"))
        R = np.load(join(dir_path, "R.npy"))
        s_0 = np.load(join(dir_path, "s_0.npy"))[0]
        return MDP(P, R, s_0=s_0)

    print("Specified directory doesn't exists!")
    return None


def load_GW(dir_path):
    """Load the Gridworld MDP saved in the directory path specified."""
    if os.path.exists(dir_path):
        P = np.load(join(dir_path, "P.npy"))
        R = np.load(join(dir_path, "R.npy"))
        map_ = np.load(join(dir_path, "map.npy"))
        meta = np.load(join(dir_path, "meta.npy"))
        s_0, p_slipping, loop = int(meta[0]), meta[1], bool(meta[2])
        goals = []
        for goal in range(3, meta.size, 3):
            goals.append(((int(meta[goal]), int(meta[goal+1])), meta[goal+2]))

        w, h = map_.shape
        walls = list(zip(*np.nonzero(1 - map_)))
        mdp = GridWorld(w, h, walls=walls, start=(s_0 % w, s_0 // w),
                        goals=goals, p_slipping=p_slipping, loop=loop)
        mdp.P, mdp.R = P, R
        return mdp

    print("Specified directory doesn't exists!")
    return None


def near_equal(v1, v2, epsilon=None):
    """Whether v1 is epsilon-close to v2. If None, equal."""
    if epsilon is None:
        return (v1 == v2).all()
    else:
        return (np.abs(v1 - v2) < epsilon).all()

def policy_kernels(mdp, policy):
    """Returns the probability and reward kernels of policy applied on mdp."""
    return mdp.P[mdp.states, policy, :], mdp.R[mdp.states, policy]


def get_maximising_policy(v, mdp, gamma=0.99):
    """Return the v-maximising policy in the given MDP."""
    return np.argmax(mdp.R + gamma * np.tensordot(mdp.P, v, axes=([2], [0])),
                     axis=1)


def get_maximising_action(s, v, mdp, gamma=0.99):
    """Return the v-maximising action in the given MDP at state s."""
    return np.argmax(mdp.R[s] + gamma * (mdp.P[s] @ v))


def generate_random_policy(S, A):
    """Generate a random policy for S states and A actions."""
    return np.random.randint(0, high=A, size=S)


def bellman_operator(v, R, P, gamma=0.99, m=1):
    """
    Apply the Bellman operator m times on values v.

    Returns R + gamma * P @ v.

    Parameters:
    ----------
    v : np.array
        Value vector.
    R : np.array
        Reward vector.
    P : np.array
        Probability matrix.
    gamma : 0. < float < 1., default 0.99
        Discount factor.
    m : positive int, default 1
        Number of times to apply the operator.
    """
    P_ = Matrix(P)
    if m == 1:  # no need to make a copy, faster computation
        return R + gamma * (P @ v)

    v_out = np.copy(v)
    for _ in range(m):
        v_out = R + gamma * (P_.vector_product(v_out))

    return v_out


def bellman_policy(v, mdp, policy, gamma=0.99, m=1):
    """
    Apply the Bellman operator associated to policy m times on values v.

    Returns R_policy + gamma * P_policy @ v.

    Parameters:
    ----------
    v : np.array
        Value vector.
    mdp : MDP instance
        MDP to use.
    policy : array-like of size mdp.S
        Deterministic, stationary policy to use. policy[s] yields the action to
        be taken from state s.
    gamma : 0. < float < 1., default 0.99
        Discount factor.
    m : positive int, default 1
        Number of times to apply the operator.
    """
    P_policy, R_policy = policy_kernels(mdp, policy)

    return bellman_operator(v, R_policy, P_policy, gamma=gamma, m=m)


def bellman_optimal(v, mdp, gamma=0.99, m=1):
    """
    Apply the optimal Bellman operator m times on values v.

    Returns max{R + gamma * P @ v} over actions.

    Parameters:
    ----------
    v : np.array
        Value vector.
    mdp : MDP instance
        MDP to use.
    gamma : 0. < float < 1., default 0.99
        Discount factor.
    m : positive int, default 1
        Number of times to apply the operator.
    """
    if m == 1:  # no need to make a copy, faster computation
        return np.max(mdp.R + gamma * np.tensordot(mdp.P, v, axes=([2], [0])),
                      axis=1)

    P, R = mdp.P.transpose((1, 0, 2)), mdp.R.transpose((1, 0))
    v_out = np.copy(v)
    P_ = Tensor(P)
    for _ in range(m):
        v_out = np.max(R + gamma * (P_.vector_product(v_out)), axis=0)

    return v_out
