"""
A showcase of what the module has to offer, and how to use it.
"""
import numpy as np
from MDP import RiverSwim
from environments import GW_random_maze, random_MDP
from policy_evaluation import FWEvaluatorOpti, VIEvaluatorOpti, LSEvaluator
from learners import MPIAgent
from renderers import GridWorldRenderer
from utility import near_equal
from experiments import PEEvaluation, PerformanceExpe


x, y = 25, 25  # width and height of the GridWorld
S = x * y
gamma = 0.98  # discount factor

# Generate MDPs
GW = GW_random_maze(x, y, random_rewards=False, loop=True, absorbing=False)
RS = RiverSwim(S, p_success=0.6, p_failure=0.05, r_left=0.01, r_right=1)
randomDense = random_MDP(S, 4, density=0.7, p_reward=0.02)
randomSparse = random_MDP(S, 4, density=0.01, p_reward=0.02)

# Compute a near-optimal policy for GW
mpi_learner = MPIAgent(GW, np.zeros(GW.S), gamma=gamma)
p_star = mpi_learner.plan()

# Render the environment and policy
renderer = GridWorldRenderer()
renderer.render_policy(GW, p_star)

# Instantiate policy evaluators for p_star
epsilon = 1e-3
FWE = FWEvaluatorOpti(GW, p_star, gamma=gamma, strategy='extend hull')
VI = VIEvaluatorOpti(GW, p_star, np.zeros(GW.S), epsilon=epsilon, gamma=gamma)

# Evaluate and compare
v1 = FWE.evaluate()
v2 = VI.evaluate()
assert near_equal(v1, v2, epsilon=epsilon)

# Visualise the algorithm process in GW
FWE.reset()
VI.reset()
v_max = v1.max()
renderer.render_value_propagation(VI, freq=1, interval=1., limits=(0, v_max),
                                  show=True)
renderer.render_value_propagation_FW(FWE, interval=1., limits=(0, v_max),
                                     show=True)

# Compare the behaviour of VI and FW on GridWorld
pe = PEEvaluation([VI, FWE])
pe.reset_all()
pe.run()
pe.plot_values_along_complexity(state=0, evaluators=None,
                                title="Values at state s0")
pe.plot_states_along_complexity(FWE, [0, S-1, S//2],
                                labels=["First state", "Last state",
                                        "Middle state"],
                                title="Values at three states during FW")

# Run experiments for performance comparison
SIZES = [(5, 5), (7, 7), (9, 9)]
MDPS = ['Gridworld', 'RiverSwim']  # 'RandomDense', 'RandomSparse']

experiment = PerformanceExpe(sizes=SIZES, mdps=MDPS, n_iter=2,
                             optimal_policy=True, gamma=0.98)
results = experiment.run(savefile=None)
experiment.print_results()
