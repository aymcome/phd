"""
Implementation of Policy Evaluation algorithms.
"""
from collections import deque
import numpy as np

from utility import policy_kernels, bellman_operator
from floyd_warshall import to_weighted_graph_with_sink, to_policy_graph
from floyd_warshall import FW, FW_step
from matrix import Matrix


class Evaluator():
    """
    Compute (an approximation of) the value vector of a policy in a MDP.

    Parameters:
    ----------
    mdp : MDP object, with number of states and actions S and A
    policy : numpy.array, with shape S
    gamma : 0 < float < 1., default 0.99
        The discount factor to use.

    Attributes:
    ----------
    m : MDP
    S : number of states
    A : number of actions
    p : policy
    gamma : the discount factor
    """
    def __init__(self, mdp, policy, gamma=0.99):
        self.m = mdp
        self.S = mdp.S
        self.A = mdp.A

        self.p = policy

        self.gamma = gamma

    def reset(self):
        self.m.reset()

    def set_policy(self, policy):
        """Set a new policy to be evaluated."""
        self.p = policy

    def get_complexity(self):
        """Return the number of basic operations executed by the algorithm."""
        return 1.

    def evaluate(self):
        """Compute and return the value vector of the policy."""
        return np.random.random(self.S)


class LSEvaluator(Evaluator):
    """
    Solve the linear Bellman equation by Gaussian elimination.

    Parameters:
    ----------
    mdp : MDP object, with number of states and actions S and A
    policy : numpy.array, with shape S
    gamma : 0 < float < 1., default 0.99
    """
    def __init__(self, mdp, policy, gamma=0.99):
        super().__init__(mdp, policy, gamma)

    def get_complexity(self):
        return self.S**3

    def evaluate(self):
        return linear_solving(self.m, self.p, gamma=self.gamma)


class FWEvaluator(Evaluator):
    """
    Implement the Floyd-Warshall policy evaluator. Naively update every
    edge weight every iteration.
    Multiple versions (cf integration order) can be specified through the
    strategy parameter. A queue of states to be integrated can be filled
    and emptied, should the user want to force a certain order.

    Parameters:
    ----------
    mdp : MDP object, with number of states and actions S and A
    policy : numpy.array, with shape S
    gamma : 0 < float < 1., default 0.99
    strategy : {None, list of states, 'eager', 'extend hull'},
    default 'extend hull'
        Specify how to choose the next integrated state. None means at random,
        a list of states adds them to the queue, 'eager' selects the state
        maximising the current value, 'extend hull' is eager inside the border.
    count_all : boolean, default False
        If False, only consider updated edges when computing the number of
        operations done in a step, otherwise count edges with values staying
        the same.
    """
    def __init__(self, mdp, policy, gamma=0.99, strategy=None, count_all=False):
        super().__init__(mdp, policy, gamma=gamma)
        self.name = "Floyd-Warshall"
        self.count_all = count_all

        # Filter masks for already integrated states
        self.integrated = np.zeros(mdp.S + 1, dtype=bool)

        # Get associated graph with discounted probabilities and reward sink
        graph = to_weighted_graph_with_sink(mdp, gamma=gamma)
        self.W_0 = to_policy_graph(graph, policy)

        self.W = self.W_0.copy()  # current weight matrix
        self.s_f = self.S  # sink state

        self.strat = strategy
        self.queue = deque()

        if isinstance(strategy, (list, np.ndarray)):
            self.strat = list(set(strategy))  # remove duplicates
            self.queue = deque(self.strat)  # states to integrate next

        elif strategy == 'eager':
            self._choose_next_state = self._choose_eagerly

        elif strategy == 'extend hull':
            self._choose_next_state = self._choose_extend_hull

        elif strategy == 'Manhattan':
            self._choose_next_state = self._choose_closest_distance

        elif strategy == 'shortest path':
            self._choose_next_state = self._choose_closest_distance
            self.compute_shortest_distance()

        elif strategy == "a star":
            self._choose_next_state = self._choose_a_star
            self.compute_shortest_distance()
            self.dists_disc = gamma ** self.dists

    def reset(self):
        self.m.reset()

        if isinstance(self.strat, (list, np.ndarray)):
            self.queue = deque(self.strat)
        else:
            self.queue = deque()

        self.integrated = np.zeros(self.S + 1, dtype=bool)

        self.W = self.W_0.copy()

    def set_policy(self, policy):
        self.p = policy

        # Get associated graph with discounted probabilities and reward sink
        graph = to_weighted_graph_with_sink(self.m, gamma=self.gamma)
        self.W_0 = to_policy_graph(graph, policy)

        self.reset()

    def step(self, n_iter=1, return_state=False):
        """Perform the next integration step."""
        n_iter = min(n_iter, self.nb_of_states_left())

        for _ in range(n_iter):
            self.s = self._get_next_state()
            self.integrate(self.s)

        if return_state:
            return self.s
        return None

    def nb_of_states_left(self):
        return np.count_nonzero(~self.integrated)

    def integrate(self, s):
        """Integrate s in Floyd-Warshall fashion."""
        assert not(self.integrated[s]), f"{s} has already been integrated!"

        self.unqueue_state(s)
        self.integrated[s] = True

        self.W = FW_step(self.W, s)
        return self.W

    def queue_state(self, s):
        """Add one state to the queue to be integrated next."""
        self.queue.append(s)

    def queue_states(self, s):
        """Add states to the queue to be integrated next."""
        self.queue.extend(s)

    def unqueue_state(self, s):
        """Remove a state from the queue."""
        try:
            self.queue.remove(s)
        except ValueError:
            pass

    def unqueue_states(self, s):
        """Remove states from the queue."""
        for i in s:
            self.unqueue_state(i)

    def clear_queue(self):
        """Remove all states from the queue."""
        self.queue = deque()

    def _get_next_state(self):
        """
        Return the next state to be integrated.
        If queue isn't empty, pop the next state. Otherwise, follow the method
        specified at initialisation.
        """
        if len(self.queue) > 0:
            return self.queue.popleft()

        return self._choose_next_state()

    def _choose_next_state(self):
        """Randomly return a state not integrated yet."""
        return np.random.choice((~self.integrated).nonzero()[0])

    def _choose_eagerly(self):
        """Choose state maximising the current value."""
        not_integr = ~self.integrated
        values = self.W[:, -1][not_integr]
        return not_integr.nonzero()[0][values.argmax()]

    def _choose_extend_hull(self):
        """Choose a state in the border maximising the current value."""
        border = self.get_border()
        if not border.any():
            return self._choose_eagerly()
        return border[np.argmax(self.get_value()[border])]

    def _choose_closest_distance(self):
        """Choose a state in the border minimising distance from s_0."""
        border = self.get_border()
        if not border.any():
            return self._choose_eagerly()
        return self.closest_state(border)

    def _choose_a_star(self):
        """Choose a state maximising the heuristic to s_0."""
        border = self.get_border()
        if not border.any():
            return self._choose_eagerly()
        heuristic = self.dists_disc[border]
        heuristic *= 1/(1 - self.W[border, border])
        heuristic *= self.W[border, self.s_f]
        return border[np.argmax(heuristic)]

    def closest_state(self, states=None):
        """Return the closest state to s_0."""
        if states is None:
            states = self.m.states
        return list(states)[np.argmin([self.distance(s) for s in states])]

    def distance(self, s):
        """Return the distance from s_0."""
        return self.m.distance(s, self.m.s0)

    def compute_shortest_distance(self):
        """Compute the shortest number of steps from s_0 for every state."""
        dists = np.inf * np.ones(self.S, dtype=int)
        visited = np.zeros(self.S, dtype=bool)
        dists[self.m.s0] = 0
        states = np.arange(self.S)

        while not visited.all():
            u = states[~visited][np.argmin(dists[~visited])]
            visited[u] = True
            neigh = self.m.get_neighbour_states(u)
            for i in neigh:
                if not visited[i]:
                    new_dist = dists[u] + 1
                    if new_dist < dists[i]:
                        dists[i] = new_dist

        self.dists = dists
        self.distance = self.get_shortest_distance

    def get_shortest_distance(self, s):
        """Get the shortest number of steps from s_0."""
        return self.dists[s]

    def get_neighbors(self, s):
        """Return the predecessors and successors of s."""
        return self.W[:, s] > 0, self.W[s, :] > 0

    def get_border(self):
        """Return the predecessors of s_f not integrated yet."""
        preds, _ = self.get_neighbors(self.s_f)
        return (preds & ~self.integrated).nonzero()[0]

    def get_step_complexity(self):
        """Return the number of updated edges in last step call."""
        preds, succs = self.get_neighbors(self.s)
        if self.count_all:
            return preds.sum() * succs.sum()
        return preds.sum() * (succs & ~self.integrated).sum()

    def evaluate(self):
        self.step(n_iter=self.S + 1)
        return self.get_value()

    def should_stop(self):
        """Return a boolean indicating if the evaluation is complete."""
        return self.integrated.all()

    def get_value(self, with_sink=False):
        """Return the current value vector."""
        if with_sink:
            return self.W[-1]
        else:
            return self.W[:-1, -1]

    def get_integrated_states(self):
        """Return an array of the states already integrated."""
        return self.integrated.nonzero()[0]

    def get_non_integrated_states(self):
        """Return an array of the states not already integrated."""
        return (~self.integrated).nonzero()[0]


class FWEvaluatorOpti(FWEvaluator):
    """
    Implement the Floyd-Warshall policy evaluator. Progressive version, update
    only the relevant edges.
    Multiple versions (cf integration order) can be specified through the
    strategy parameter. A queue of states to be integrated can be filled
    and emptied, should the user want to force a certain order.

    Parameters:
    ----------
    mdp : MDP object, with number of states and actions S and A
    policy : numpy.array, with shape S
    gamma : 0 < float < 1., default 0.99
    strategy : {None, list of states, 'eager', 'extend hull'},
    default 'extend hull'
        Specify how to choose the next integrated state. None means at random,
        a list of states adds them to the queue, 'eager' selects the state
        maximising the current value, 'extend hull' is eager inside the border.
    compute_all : boolean, default False
        If True, maintain every edge weight through the iterations; otherwise,
        update only the edges relevant for the value computation.
    """
    def __init__(self, mdp, policy, gamma=0.99, strategy=None,
                 compute_all=False):
        super().__init__(mdp, policy, gamma=gamma, strategy=strategy)

        self.compute_all = compute_all

        self.not_integrated = set(range(self.S))
        self.preds, self.succs = self.compute_neighbors()
        self.s_f = self.S

        self.step_cost = 0

        del self.integrated

    def reset(self):
        super().reset()
        self.not_integrated = set(range(self.S))
        self.preds, self.succs = self.compute_neighbors()
        self.s_f = self.S

        del self.integrated

    def compute_neighbors(self):
        """Return the adjacency lists."""
        preds = [set() for _ in range(self.S + 1)]
        succs = [set() for _ in range(self.S + 1)]
        edges = self.W.nonzero()

        for s, s_ in zip(*edges):
            preds[s_].add(s)
            succs[s].add(s_)
        return preds, succs

    def nb_of_states_left(self):
        return len(self.not_integrated)

    def integrate(self, s):
        """Integrate s in Floyd-Warshall fashion."""
        assert s in self.not_integrated, f"{s} has already been integrated!"

        self.unqueue_state(s)
        self.not_integrated.remove(s)

        self.FW_step(s)
        return self.W

    def FW_step(self, s):
        """Update edges weights by integrating s."""
        c = 0
        if self.compute_all:
            targets = self.succs[s]
        else:
            targets = self.succs[s] & self.not_integrated
        targets.add(self.s_f)

        loop_on_s = 1/(1 - self.W[s, s])
        w_to_s = self.W[list(self.preds[s]), s]
        w_from_s = self.W[s, list(targets)]
        for u, w_u in zip(self.preds[s], w_to_s):
            for v, w_v in zip(targets, w_from_s):
                self.W[u, v] += w_u * loop_on_s * w_v
                self.preds[v].add(u)
                self.succs[u].add(v)
                c += 1

        self.step_cost = c

    def _choose_next_state(self):
        """Randomly return a state not integrated yet."""
        return np.random.choice(self.not_integrated)

    def _choose_eagerly(self):
        """Choose state maximising the current value."""
        not_integr = list(self.not_integrated)
        values = self.W[not_integr, -1]
        return not_integr[values.argmax()]

    def _choose_extend_hull(self):
        """Choose a state in the border."""
        preds = list(self.preds[self.s_f] & self.not_integrated)
        if not preds:
            return self._choose_eagerly()
        return preds[self.W[preds, -1].argmax()]

    def _choose_closest_distance(self):
        """Choose a state in the border minimising distance from s_0."""
        border = self.get_border()
        if not border:
            return self._choose_eagerly()
        return self.closest_state(border)

    def _choose_a_star(self):
        """Choose a state maximising the heuristic to s_0."""
        border = list(self.get_border())
        if not border:
            return self._choose_eagerly()
        heuristic = self.dists_disc[border]
        # heuristic *= 1/(1 - self.W[border, border])
        heuristic *= self.W[border, self.s_f]
        return border[np.argmax(heuristic)]

    def get_neighbors(self, s):
        """Return the predecessors and succesors of s."""
        return self.preds[s], self.succs[s]

    def get_step_complexity(self):
        """Return the number of updated edges in the last step call."""
        return self.step_cost

    def should_stop(self):
        """Return a boolean indicating if the evaluation is complete."""
        return len(self.not_integrated) == 0

    def get_border(self):
        """Return the predecessors of s_f not integrated yet."""
        preds, _ = self.get_neighbors(self.s_f)
        return preds & self.not_integrated

    def get_integrated_states(self):
        """Return a set of the states already integrated."""
        return set(range(self.S)) - self.not_integrated


class VIEvaluator(Evaluator):
    """
    Implement the Value Iteration policy evaluator. Naive version, update
    values with every state.

    Parameters:
    ----------
    mdp : MDP object, with number of states and actions S and A
    policy : numpy.array, with shape S
    v_0 : numpy.array, with shape S
        The initial value vector to be updated.
    epsilon : 0 < float, default 0.01
        Error threshold for the stopping criterion defined in
        [Puterman 1994].
    gamma : 0 < float < 1, default 0.99
        Discount factor.
    max_iter : int, default np.inf
        Maximum number of iterations to perform.
    """
    def __init__(self, mdp, policy, v_0, epsilon=0.01, gamma=0.99,
                 max_iter=np.inf):
        super().__init__(mdp, policy, gamma=gamma)
        self.name = "Value Iteration"

        assert v_0.size == mdp.S, f"Init vector should be of size {mdp.S}"
        self.v_0 = v_0

        self.eps = epsilon
        self.max_iter = max_iter
        self.thres = epsilon * (1-gamma)/gamma  # threshold for epsilon error

        self.n = 0
        self.norm = np.linalg.norm

        # Value vectors used in the value iteration
        self.v_1, self.v_2 = v_0.copy() + 2 * self.thres, v_0.copy()
        # Array and Matrix used in the value iteration
        self.P, self.R = policy_kernels(mdp, policy)
        self.P_ = Matrix(self.P)

    def reset(self):
        self.m.reset()
        self.n = 0
        v_0 = self.v_0
        self.v_1, self.v_2 = v_0.copy() + 2 * self.thres, v_0.copy()

    def set_policy(self, policy):
        self.p = policy

        # Array and Matrix used in the value iteration
        self.P, self.R = policy_kernels(self.m, policy)
        self.P_ = Matrix(self.P)

        self.reset()

    def step(self, n_iter=1):
        """Perform n_iter value iterations. Doesn't go over max_iter."""
        n = self.n
        max_iter = self.max_iter

        if n >= max_iter:
            print(f"Maximum number of cumulated iterations {max_iter} reached")

        n_iter = min(n + n_iter, max_iter)
        norm = self.norm
        thres = self.thres
        v_1, v_2 = self.v_1, self.v_2

        while (n < n_iter) and (norm(v_2 - v_1, np.inf) >= thres):
            v_1 = v_2
            v_2 = self.bellman_operator(v_1)
            n += 1

        self.v_1, self.v_2 = v_1, v_2
        self.n = n

    def bellman_operator(self, v):
        """Apply the policy Bellman operator on the value vector v."""
        return self.R + self.gamma * (self.P_.vector_product(v))

    def evaluate(self):
        self.step(n_iter=self.max_iter)
        return self.get_value()

    def get_step_complexity(self):
        """Return the number of edges used in the previous step call."""
        return self.S**2

    def should_stop(self):
        """Return a boolean indicating if the evaluation is completed."""
        return ((self.n >= self.max_iter) |
                (self.norm(self.v_2 - self.v_1, np.inf) < self.thres))

    def get_value(self):
        """Return the current value vector."""
        return self.v_2


class VIEvaluatorOpti(VIEvaluator):
    """
    Implement the Value Iteration policy evaluator. Look only at succesors for
    value update, not all the states.
    If the MDP has a 'get_neighbour_states' method, use it to get the
    neighbours of a state; otherwise compute them from the probability kernel.

    Parameters:
    ----------
    mdp : MDP object, with number of states and actions S and A
    policy : numpy.array, with shape S
    v_0 : numpy.array, with shape S
        The initial value vector to be updated.
    epsilon : 0 < float, default 0.01
        Error threshold for the stopping criterion defined in
        [Puterman 1994].
    gamma : 0 < float < 1, default 0.99
        Discount factor.
    max_iter : int, default np.inf
        Maximum number of iterations to perform.
    """
    def __init__(self, mdp, policy, v_0, epsilon=0.01, gamma=0.99,
                 max_iter=np.inf):
        # if hasattr(mdp, "get_neighbour_states"):
        #     self.get_neighbour_states = mdp.get_neighbour_states

        super().__init__(mdp, policy, v_0, epsilon=epsilon,
                         gamma=gamma, max_iter=max_iter)

        self.succs = [np.nonzero(self.P[s, :])[0] for s in self.m.states]

        self.name = "Value Iteration"
        self.step_cost = 0

    def get_neighbour_states(self, s):
        """Return the list of succesors of s."""
        return self.succs[s]

    def bellman_operator(self, v):
        """Bellman operator using only neighbours of each state."""
        R, P, gamma = self.R, self.P, self.gamma

        new_v = R.copy()
        c = 0
        for s in self.m.states:
            neigh = self.get_neighbour_states(s)
            for n in neigh:
                new_v[s] += gamma * P[s, n] * v[n]
            c += len(neigh)

        self.step_cost = c
        return new_v

    def get_step_complexity(self):
        """Return the number of edges used in the previous step call."""
        return self.step_cost


class VIEvaluatorPower(VIEvaluator):
    """
    Power method for Value Iteration.

    Disclaimer: not good at all, very slow.
    """
    def __init__(self, mdp, policy, v_0, epsilon=0.01, gamma=0.99,
                 max_iter=np.inf):
        super().__init__(mdp, policy, v_0, epsilon=epsilon,
                         gamma=gamma, max_iter=max_iter)

        self.n = 1
        self.succs = [np.nonzero(self.P[s, :])[0] for s in self.m.states]
        self.succs_ = [np.nonzero(self.P[s, :])[0] for s in self.m.states]
        self.probas = self.P.copy()
        self.probas_ = self.P.copy()
        self.v_0 = self.R.copy()
        self.v_1, self.v_2 = self.R.copy(), self.R.copy()

        self.name = "Value Iteration"
        self.step_cost = 0

    def reset(self):
        self.m.reset()
        self.n = 1
        self.succs = [set(np.nonzero(self.P[s, :])[0]) for s in self.m.states]
        self.succs_ = [set() for s in self.m.states]
        self.probas = self.P.copy()
        self.probas_ = np.zeros(self.P.shape)
        self.v_1, self.v_2 = self.R.copy(), self.R.copy()

    def step(self, n_iter=1):
        """Perform n_iter value iterations. Doesn't go over max_iter."""
        n = self.n
        max_iter = self.max_iter

        if n >= max_iter:
            print(f"Maximum number of cumulated iterations {max_iter} reached")

        n_iter = min(n + n_iter, max_iter)
        norm = self.norm
        thres = self.thres
        v_1, v_2 = self.v_1, self.v_2
        probas, probas_ = self.probas, self.probas_
        succs, succs_ = self.succs, self.succs_
        discount, states = self.gamma**n, self.m.states

        while (n < n_iter) and ((norm(v_2 - v_1, np.inf) >= thres) or n == 1):
            if n >= self.m:
                v_1 = v_2
                v_2 = self.bellman_operator(v_1)
                n += n

            c = 0
            v_1 = v_2.copy()
            for s in states:
                for s_ in succs[s]:
                    p_i = probas[s, s_]
                    v_2[s] += discount * p_i * v_1[s_]
                    for s__ in succs[s_]:
                        probas_[s, s__] += p_i * probas[s_, s__]
                        succs_[s].add(s__)
                        c += 1
            probas = probas_.copy()
            probas_ = np.zeros(self.P.shape)
            succs = [succs_[s].copy() for s in states]
            succs_ = [set() for s in states]

            self.step_cost = c
            n *= 2
            discount *= discount

        self.v_1, self.v_2 = v_1, v_2
        self.probas, self.probas_ = probas, probas_
        self.succs, self.succs_ = succs, succs_
        self.n = n

    def get_neighbour_states(self, s):
        """Return the list of succesors of s."""
        return self.succs[s]

    def bellman_operator(self, v):
        """Bellman operator using only neighbours of each state."""
        R, P, gamma = self.R, self.probas, self.gamma ** self.m

        new_v = R.copy()
        c = 0
        for s in self.m.states:
            neigh = self.get_neighbour_states(s)
            for n in neigh:
                new_v[s] += gamma * P[s, n] * v[n]
            c += len(neigh)

        self.step_cost = c
        return new_v

    def get_step_complexity(self):
        """Return the number of edges used in the previous step call."""
        return self.step_cost

    def should_stop(self):
        """Return a boolean indicating if the evaluation is completed."""
        return ((self.n >= self.max_iter) |
                (self.norm(self.v_2 - self.v_1, np.inf) < self.thres) |
                self.n == 1)


class VIEvaluatorPower2(VIEvaluator):
    """
    Power method for Value Iteration, 2nd implementation.

    Disclaimer: not good at all, very slow.
    """
    def __init__(self, mdp, policy, v_0, epsilon=0.01, gamma=0.99,
                 max_iter=np.inf, m=8):
        super().__init__(mdp, policy, v_0, epsilon=epsilon,
                         gamma=gamma, max_iter=max_iter)

        self.n = 1
        self.succs = [np.nonzero(self.P[s, :])[0] for s in self.m.states]
        self.succs_ = [np.nonzero(self.P[s, :])[0] for s in self.m.states]
        self.probas = self.P.copy()
        self.probas_ = self.P.copy()
        self.v_0 = self.R.copy()
        self.v_1, self.v_2 = self.R.copy(), self.R.copy()

        self.name = "Value Iteration"
        self.step_cost = 0

    def reset(self):
        self.m.reset()
        self.n = 1
        self.succs = [set(np.nonzero(self.P[s, :])[0]) for s in self.m.states]
        self.succs_ = [set() for s in self.m.states]
        self.probas = self.P.copy()
        self.probas_ = np.zeros(self.P.shape)
        self.v_1, self.v_2 = self.R.copy(), self.R.copy()

    def step(self, n_iter=1):
        """Perform n_iter value iterations. Doesn't go over max_iter."""
        n = self.n
        max_iter = self.max_iter

        if n >= max_iter:
            print(f"Maximum number of cumulated iterations {max_iter} reached")

        n_iter = min(n + n_iter, max_iter)
        norm = self.norm
        thres = self.thres
        v_1, v_2 = self.v_1, self.v_2
        probas, probas_ = self.probas, self.probas_
        succs, succs_ = self.succs, self.succs_
        discount, states = self.gamma**n, self.m.states

        while (n < n_iter) and ((norm(v_2 - v_1, np.inf) >= thres) or n == 1):
            c = 0
            v_1 = v_2.copy()
            for s in states:
                for s_ in succs[s]:
                    p_i = probas[s, s_]
                    v_2[s] += discount * p_i * v_1[s_]
                    for s__ in succs[s_]:
                        probas_[s, s__] += p_i * probas[s_, s__]
                        succs_[s].add(s__)
                        c += 1
            probas = probas_.copy()
            probas_ = np.zeros(self.P.shape)
            succs = [succs_[s].copy() for s in states]
            succs_ = [set() for s in states]

            self.step_cost = c
            n *= 2
            discount *= discount

        self.v_1, self.v_2 = v_1, v_2
        self.probas, self.probas_ = probas, probas_
        self.succs, self.succs_ = succs, succs_
        self.n = n

    def get_step_complexity(self):
        """Return the number of edges used in the previous step call."""
        return self.step_cost

    def should_stop(self):
        """Return a boolean indicating if the evaluation is completed."""
        return ((self.n >= self.max_iter) |
                (self.norm(self.v_2 - self.v_1, np.inf) < self.thres) |
                self.n == 1)


# Functional approach


def linear_solving(mdp, policy, gamma=0.99):
    """
    Solve Bellman equation with matrix inversion.

    Returns the value array of the given policy on the MDP.

    Parameters:
    ----------
    mdp : MDP instance
        MDP for the policy to be evaluated on.
    policy : array-like of size mdp.S
        Deterministic, stationary policy to be evaluated. policy[s] yields the
        action to be taken from state s.
    gamma : 0. < float < 1., default 0.99
        Discount factor.
    """
    P_policy, R_policy = policy_kernels(mdp, policy)

    parameters = Matrix(np.eye(mdp.S) - gamma * P_policy)
    return parameters.invert().m @ R_policy


def value_iteration(mdp, policy, v_0, epsilon=0.01, n_iter=np.inf, gamma=0.99):
    """
    Iterate the policy Bellman Operator until the error threshold is met.

    Returns the value array of the given policy on the MDP.

    Parameters:
    ----------
    mdp : MDP instance
        MDP for the policy to be evaluated on.
    policy : array-like of size mdp.S
        Deterministic, stationary policy to be evaluated. policy[s] yields the
        action to be taken from state s.
    v_0 : np.array of size mdp.S
        Initial value vector to iterate from.
    epsilon : float, default 0.01
        Error threshold for the infinity norm.
    n_iter : {np.inf, integer}, default np.inf
        Maximum number of iterations to perform, possible infinite.
    gamma : 0. < float < 1., default 0.99
        Discount factor.
    """
    norm = np.linalg.norm
    thres = epsilon * (1 - gamma) / gamma  # threshold to get epsilon error

    v_1, v_2 = v_0.copy() + 2 * thres, v_0.copy()
    P_policy, R_policy = policy_kernels(mdp, policy)

    n = 0
    while (n < n_iter) and (norm(v_2 - v_1, np.inf) >= thres):
        v_1 = v_2
        v_2 = bellman_operator(v_1, R_policy, P_policy, gamma=gamma, m=1)
        n += 1

    return v_2


def FWPE(mdp, policy, order=None, gamma=0.99):
    """
    Perform Floyd-Warshall Policy Evaluation on MDP for policy.

    Returns the corresponding value vector.

    Parameters:
    ----------
    mdp : MDP instance
        MDP to use.
    policy : array-like of size mdp.S
        Deterministic, stationary policy to use. policy[s] yields the action to
        be taken from state s.
    order : np.array, default None
        Permutation of the states of the MDP. Gives the order of integration of
        the states. If None, use the mdp natural ordering.
    gamma : 0. < float < 1., default 0.99
        Discount factor.
    """
    # Get associated graph with discounted probabilities and reward sink
    graph = to_weighted_graph_with_sink(mdp, gamma=gamma)
    graph = to_policy_graph(graph, policy)

    W = FW(graph, order=order)  # FW integration
    return W[:-1, -1]  # values correspond to weights towards sink state
