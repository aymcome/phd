"""
Homemade definition of matrices, and implementation of the basic operations.
"""
import numpy as np


class Matrix():
    """
    Square matrix.

    Parameters:
    ----------
    value : float, int or a np.ndarray, default 0
        The value to populate the matrix with. If a float, populate the whole
        matrix with that value. value[i][j] should return the element in ith
        row, jth column.
    n : int > 0, default 1
        Number of rows and columns.

    Attributes:
    ----------
    m : the numpy array of values -> m[i][j]
    n : the size of the squared matrix
    shape : (n, n)

    Methods:
    -------
    add
    multiply
    scalar_product
    vector_product
    invert
    at
    set
    transpose
    get_row
    get_column
    to_zero
    to_one
    to_identity
    copy
    print_matrix
    """

    def __init__(self, value=0, n=1):
        if isinstance(value, (float, int)):
            self.m = [n * [value] for _ in range(n)]
            self.n = n
            self.shape = (n, n)

        elif isinstance(value, np.ndarray):
            n = value.shape[0]
            assert value.shape == (n, n), f"Not a square matrix: {value.shape}"
            self.m = value
            self.n = n
            self.shape = (n, n)
        else:
            raise TypeError("value should be a float, int or numpy array")

    def at(self, row, column):
        """Select an element from the matrix."""
        return self.m[row][column]

    def set(self, row, column, value):
        """Set the value at (row, column) to value."""
        self.m[row][column] = value

    def transpose(self, inplace=False):
        """Return the transpose matrix."""
        return self.m.T

    def get_row(self, row):
        return self.m[row, :]

    def get_column(self, col):
        return self.m[:, col]

    def add(self, a, inplace=True):
        """Add matrix a."""
        self._check_compatibility(a)

        n = self.n
        m1, m2 = self.m, a.m
        result = np.array([[m1[i][j] + m2[i][j] for i in range(n)]
                           for j in range(n)])

        if inplace:
            self.m = result
        else:
            return Matrix(result)

    def __add__(a, b):
        return a.add(b, inplace=False)

    def multiply(self, a, inplace=True):
        """Matrix product with a."""
        self._check_compatibility(a)

        n = self.n
        m1, m2 = self.m, a.m

        result = np.array([[sum(m1[k][i] * m2[j][k] for k in range(n))
                           for i in range(n)]
                          for j in range(n)])

        if inplace:
            self.m = result
        else:
            return Matrix(result)

    def __mul__(a, b):
        return a.multiply(b, inplace=False)

    def scalar_product(self, scalar, inplace=True):
        """Scalar product with float/int scalar."""
        if inplace:
            self.m = scalar * self.m
        else:
            return Matrix(scalar * self.m)

    def vector_product(self, vector: np.ndarray):
        """Return the numpy array of the vector product."""
        n, m = self.n, self.m
        assert vector.size == n, f"vector should be of shape {n}"

        return np.array([sum(m[i, j] * vector[j] for j in range(n))
                         for i in range(n)])

    def invert(self):
        """Return the inverted matrix. Diagonal elements must be non-zero."""
        m, n = self.m.copy(), self.n
        result = np.eye(n)

        for i in range(n):
            pivot = m[i][i]
            for j in range(n):
                m[i][j] /= pivot
                result[i][j] /= pivot

            for j in range(n):
                if j != i:
                    scaler = m[j][i]
                    for k in range(n):
                        m[j][k] -= scaler * m[i][k]
                        result[j][k] -= scaler * result[i][k]

        return Matrix(result)

    def to_zero(self):
        self.m = np.zeros((self.n, self.n))

    def to_one(self):
        self.m = np.ones((self.n, self.n))

    def to_identity(self):
        self.m = np.eye((self.n, self.n))

    def copy(self):
        return Matrix(self.m.copy())

    def print_matrix(self):
        print(self.m)

    def _check_compatibility(self, a):
        """Check if a is a Matrix of same format."""
        assert isinstance(a, Matrix), "a should be a Matrix"
        assert self.n == a.n, f"Matrices must have same size: {self.n}!={a.n}"


class Tensor():
    """3D array, behaves like a list of Matrix."""
    def __init__(self, tensor: np.ndarray):
        shape = tensor.shape
        assert (len(shape) == 3) and (shape[1] == shape[2]), (
            "Input tensor should be 3D with same 2nd and third dimension size")

        self.n = shape[1]
        self.k = shape[0]
        self.t = tensor

    def vector_product(self, vector: np.ndarray):
        """
        Return the numpy matrix of the vector product on the last two
        dimensions.
        """
        t, n, k = self.t, self.n, self.k
        assert vector.size == n, f"Vector should be of shape {n}"

        return np.array([[sum(t[a, i, j] * vector[j] for j in range(n))
                          for i in range(n)]
                         for a in range(k)])
