"""
Definition of the MDP class, for discrete Markov Decision Processes, plus some
utility functions to quickly instantiate classic MDPs.
"""
import os
from os.path import join
import numpy as np
import numpy.random as npr


class MDP():
    """
    Discrete MDP.

    Only deterministic rewards are considered, as we're interested in the
    expectation. States and actions are identified to their indices.

    Parameters:
    ----------
    P : (S, A, S) np.array
        Transition matrix. P[s, a, s'] is the probability of the agent moving
        to state s' when taking action a from state s.
    R : (S, A) np.array
        Expected reward. R[s, a] is the expected reward received by the agent
        when taking action a from state s.
    s_0 : int between 0 and S-1, default 0
        Starting state.

    Methods:
    -------
    reset
    step
    sample_next_state
    """

    def __init__(self, P, R, s_0=0):
        self.P, self.R = P, R
        self.S, self.A, _ = P.shape

        self.states = np.arange(self.S)
        self.actions = np.arange(self.A)

        assert 0 <= s_0 < self.S
        self.s0 = s_0
        self.s = s_0

        self.reset()  # set to initial state

    def reset(self):
        """Reset environment to original state."""
        self.s = self.s0
        return self.s

    def step(self, a):
        """Perform action a from current state."""
        s = self.s

        self.s = npr.choice(self.states, p=self.P[s, a])
        r = self.R[s, a]

        return self.s, r

    def save(self, path="saves/MDP"):
        """Save the MDP in the directory specified, to be reloaded later."""
        if not os.path.exists(path):
            os.makedirs(path)
            np.save(join(path, "P"), self.P)
            np.save(join(path, "R"), self.R)
            np.save(join(path, "s_0"), np.array([self.s0]))
        else:
            print("Directory already exists!")

    def save_overwrite(self, path="saves/MDP"):
        """Save the MDP in the specified directory, and overwrite if needed."""
        if not os.path.exists(path):
            os.makedirs(path)
        else:
            print("Directory already exists, overwriting!")

        np.save(join(path, "P"), self.P)
        np.save(join(path, "R"), self.R)
        np.save(join(path, "s_0"), np.array([self.s0]))


class RiverSwim(MDP):
    """
    River Swim MDP: two actions on a chain of states, with rewards only at the
    rightmost and leftmost states.
    Actions 0 and 1 correspond to go right and go left respectively.
    Going left is always successful.

    Parameters:
    ----------
    S : 0 < int
        The number of states.
    p_success : float < 1, default 0.6
        Probability of going right when taking action go right.
    p_failure : float < 1, default 0.05
        Probability of going left when taking action go right.
    r_left : float, default 0.01
        Reward collecteod at the leftmost state when going left.
    r_right : float, default 1.
        Reward collected at the rightmost state when going right.
    """
    def __init__(self, S, p_success=0.6, p_failure=0.05, r_left=0.01,
                 r_right=1):
        p_inplace = 1 - (p_success + p_failure)  # probability of not moving

        # River swim transition matrix
        P = np.zeros((S, 2, S))

        # Going left: probability 1 to go to previous state
        P[:, 0, :] += np.eye(S, k=-1)

        # Going right: go right, left or stay in place
        P[:, 1, :] += (p_success * np.eye(S, k=1)
                       + p_failure * np.eye(S, k=-1)
                       + p_inplace * np.eye(S, k=0))

        # Edge cases
        # Stay in place when going left from leftmost state
        P[0, 0, 0] = 1
        P[0, 1, 0] += p_failure
        # Stay in place when going right from rightmost state
        P[-1, 1, -1] += p_success

        # Rewards
        R = np.zeros((S, 2))
        R[S - 1, 1] = r_right
        R[0, 0] = r_left

        super().__init__(P, R, s_0=0)

        self.p_success = p_success
        self.p_failure = p_failure
        self.p_inplace = p_inplace
        self.r_left = r_left
        self.r_right = r_right

    def get_neighbour_states(self, s):
        """Return the list of successors of s."""
        neigh = [s]
        if s != 0:
            neigh.append(s-1)
        if s != self.S - 1:
            neigh.append(s+1)
        return neigh

    def distance(self, s, s_):
        """Return the number of steps between two states."""
        return np.abs(s - s_)


class GridWorld(MDP):
    """
    GridWorld MDP: navigation in a 2D grid of states, with walls and goal
    states.
    Actions 0, 1, 2, 3 correspond to go up, right, down, left respectively.
    When encoutering a wall, bounce back into place.
    Possibility to teleport the agent back to the start after reaching any
    goal state.

    Parameters:
    ----------
    w : 0 < int
        Width of the grid.
    h : 0 < int
        Height of the grid
    walls : list of coordinates, default []
        List of coordinates (i, j) for walls.
    start : tuple (int, int), default (0, 0)
        Coordinates of the starting state.
    goals : list of (coordinates, reward), default []
        Goal states to reach, giving rewards for any action.
    p_slipping : 0 <= float <= 1, default 0.3
        Probability of going in any other direction than specified.
    loop : boolean default False
        Go back to the starting state after collecting the reward in any goal
        state.
    """
    def __init__(self, w, h, walls=[], start=(0, 0), goals=[], p_slipping=0.3,
                 loop=False):
        S = w * h
        p_success, p_wrong = 1 - p_slipping, p_slipping/3

        self.w, self.h = w, h
        self.walls = walls
        self.goals = goals
        self.start = (int(start[0]), int(start[1]))
        self.p_slipping = p_slipping
        self.p_success = p_success
        self.p_wrong = p_wrong

        self.loop = loop

        index_0 = self.coord_to_state(*start)
        self.s_0 = index_0
        
        # Transition matrix
        P = np.zeros((S, 4, S))

        # Marking target states for each action
        up = np.diag(np.ones(S-w), k=w)
        right = np.diag(np.ones(S-1), k=1)
        down = np.diag(np.ones(S-w), k=-w)
        left = np.diag(np.ones(S-1), k=-1)

        # Edge states: stay inplace
        up[-w:, :] = np.eye(S)[-w:, :]
        right[w-1::w, :] = np.eye(S)[w-1::w, :]
        down[:w, :] = np.eye(S)[:w, :]
        left[::w, :] = np.eye(S)[::w, :]

        # Not slipping
        P[:, 0, :] = p_success * up
        P[:, 1, :] = p_success * right
        P[:, 2, :] = p_success * down
        P[:, 3, :] = p_success * left

        # Slipping
        P[:, 0, :] += p_wrong * (right + down + left)
        P[:, 1, :] += p_wrong * (up + down + left)
        P[:, 2, :] += p_wrong * (up + right + left)
        P[:, 3, :] += p_wrong * (up + right + down)

        # Walls
        for (i, j) in walls:
            index = self.coord_to_state(i, j)
            # Inaccessible - stay in place instead
            P[range(S), :, range(S)] += P[range(S), :, index]
            P[range(S), :, index] = 0.
            # Loop inplace
            P[index, :, :] = 0.
            P[index, :, index] = 1.

        # Rewards
        R = np.zeros((S, 4))

        if loop:
            teleport_proba = np.eye(S)[index_0, :]

        self.loop_states = []
        for (i, j), r in goals:
            index = self.coord_to_state(i, j)
            self.loop_states.append(index)
            R[index, :] = r  # get reward
            if loop:
                P[index, :, :] = teleport_proba  # go back to s_0

        super().__init__(P, R, s_0=index_0)
        self.construct_map()

    def construct_map(self):
        """Construct and store the map of walls and tiles in self.map."""
        map_ = np.ones((self.w, self.h))
        for (i, j) in self.walls:
            map_[i, j] = 0

        self.map = map_
        return map_

    def coord_to_state(self, i, j):
        """Return the ID of the state at coordinates (i, j)."""
        return i + self.w * j

    def state_to_coord(self, s):
        """Return the coordinates of state s."""
        return (s % self.w, s // self.w)

    def get_neighbour_states(self, s):
        """Return the list of successors of state s."""
        return np.unique(self.P[s].nonzero()[1])

    def distance(self, s, s_):
        """Return the Manhattan distance between two states."""
        i, j = self.state_to_coord(s)
        i_, j_ = self.state_to_coord(s_)
        return np.abs(i - i_) + np.abs(j - j_)

    def save(self, path="saves/GW"):
        """Save the MDP in the directory specified, to be reloaded later."""
        super().save(path=path)
        np.save(join(path, "map"), self.map)
        meta = [self.s0, self.p_slipping, self.loop]
        for (i, j), r in self.goals:
            meta.append(i)
            meta.append(j)
            meta.append(r)
        np.save(join(path, "meta"), meta)

    def save_overwrite(self, path="saves/GW"):
        """Save the MDP in the specified directory, and overwrite if needed."""
        super().save_overwrite(path=path)
        np.save(join(path, "map"), self.map)
        meta = [self.s0, self.p_slipping, self.loop]
        for (i, j), r in self.goals:
            meta.append(i)
            meta.append(j)
            meta.append(r)
        np.save(join(path, "meta"), meta)


# Functional approach


def river_swim(S, p_success=0.6, p_failure=0.05, r_left=0.01, r_right=1):
    """
    Return a MDP instance for River Swim.

    River Swim is a chain MDP representing a swimmer trying to go upstream.
    Start is a the leftmost state, action 0 is "swim left" and action 1 is
    "swim right". Swimming left is guaranteed, but swimming right, against
    the flow, isn't.

    Parameters:
    ----------
    S : positive int
        The number of states.
    p_success & p_failure : float between 0 and 1
        The probability of going right and left respectively, when taking
        action "swim right". Sum should be <= 1.
    r_left & r_right : positive int
        Rewards obtained at the leftmost and rightmost states respectively.
    """
    p_inplace = 1 - (p_success + p_failure)  # probability of not moving

    # River swim transition matrix
    P = np.zeros((S, 2, S))

    # Going left: probability 1 to go to previous state
    P[:, 0, :] += np.eye(S, k=-1)

    # Going right: go right, left or stay in place
    P[:, 1, :] += (p_success * np.eye(S, k=1)
                   + p_failure * np.eye(S, k=-1)
                   + p_inplace * np.eye(S, k=0))

    # Edge cases
    # Stay in place when going left from leftmost state
    P[0, 0, 0] = 1
    P[0, 1, 0] += p_failure
    # Stay in place when going right from rightmost state
    P[-1, 1, -1] += p_success

    # Rewards
    R = np.zeros((S, 2))
    R[S - 1, 1] = r_right
    R[0, 0] = r_left

    return MDP(P, R, s_0=0)


def grid_world(w, h, walls=[], start=(0, 0), goals=[], p_slipping=0.):
    """
    Return a MDP instance for Grid World.

    GridWorld is a 2D w x h grid with walls in which the agent can move in all
    four directions, and collect rewards at goal states before being teleported
    back to the initial state.
    Wall states are left as isolated states.
    Actions up, right, down, left correspond to 0, 1, 2, 3 respectively.
    State with coordinate (i, j) is encoded by i + w * j. So visually, state 0
    corresponds to lower left, and state w-1 corresponds to lower right.

    Parameters:
    ----------
    w & h : int
        Width and Height of the grid.
    walls : list of 2 ints tuples
        List of coordinates of walled positions, on which the agent can't go.
    start : 2 ints tuple
        Coordinates of the starting position.
    goals : list of (coordinates, float)
        Coordinates and rewards associated to goal states. Upon taking any
        action from one of these positions, the agent receives the specified
        reward and is sent back to the initial state.
    p_slipping : float, between 0. and 1.
        Probability to move in a direction different from the chosen one.
    """
    S = w * h
    p_success, p_wrong = 1 - p_slipping, p_slipping/3
    index_0 = start[0] + w * start[1]

    # Transition matrix
    P = np.zeros((S, 4, S))

    # Marking target states for each action
    up = np.diag(np.ones(S-w), k=w)
    right = np.diag(np.ones(S-1), k=1)
    down = np.diag(np.ones(S-w), k=-w)
    left = np.diag(np.ones(S-1), k=-1)

    # Edge states: stay inplace
    up[-w:, :] = np.eye(S)[-w:, :]
    right[w-1::w, :] = np.eye(S)[w-1::w, :]
    down[:w, :] = np.eye(S)[:w, :]
    left[::w, :] = np.eye(S)[::w, :]

    # Not slipping
    P[:, 0, :] = p_success * up
    P[:, 1, :] = p_success * right
    P[:, 2, :] = p_success * down
    P[:, 3, :] = p_success * left

    # Slipping
    P[:, 0, :] += p_wrong * (right + down + left)
    P[:, 1, :] += p_wrong * (up + down + left)
    P[:, 2, :] += p_wrong * (up + right + left)
    P[:, 3, :] += p_wrong * (up + right + down)

    # Walls
    for (i, j) in walls:
        index = i + w * j
        # Inaccessible - stay in place instead
        P[range(S), :, range(S)] += P[range(S), :, index]
        P[range(S), :, index] = 0.
        # Loop inplace
        P[index, :, :] = 0.
        P[index, :, index] = 1.

    # Rewards
    R = np.zeros((S, 4))
    teleport_proba = np.eye(S)[index_0, :]

    for (i, j), r in goals:
        index = i + w * j
        R[index, :] = r  # get reward
        P[index, :, :] = teleport_proba  # go back to s_0

    return MDP(P, R, s_0=index_0)
