"""
Render the MDPs and the value heatmaps.
"""
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import animation
from matplotlib.patches import FancyArrow, Circle
from policy_evaluation import FWEvaluator


class GridWorldRenderer():
    def __init__(self):
        # self.cmap = cmap
        pass

    def render(self, grid, cbar=True, show=True, cmap='hot'):
        """Render a grid environment in a plot."""
        if isinstance(grid, np.ma.MaskedArray):  # put masked states in  black
            cmap = plt.get_cmap(cmap)
            cmap.set_bad(color='black')

        fig, ax = plt.subplots()

        mat = ax.matshow(self.format_grid_for_plot(grid), cmap=cmap,
                         origin='lower')
        if cbar:
            fig.colorbar(mat)
        if show:
            plt.show()
        else:
            return fig, ax

    def format_grid_for_plot(self, grid):
        """Reshape the grid array for an accurate plot."""
        return grid.T

    def render_env(self, env):
        """Render a GridWorld MDP."""
        grid = self.get_map(env)
        fig, ax = self.render(grid, cbar=False, show=False)

        for (i, j), r in env.goals:
            ax.annotate(str(round(r, 2)), (i - 0.4, j - 0.2))
        plt.show()

    def get_map(self, env, mask_walls=False):
        """Return the map array with starting, rewarding and wall states."""
        grid = env.map.copy()

        grid[env.start] = 0.7
        grid[[i for (i, _), _ in env.goals],
             [j for (_, j), _ in env.goals]] = 0.5

        if mask_walls:
            return np.ma.masked_where(grid == 0, grid)
        return grid

    def render_policy(self, env, policy):
        """Render the deterministic policy as arrows on the GridWorld."""
        assert policy.size == env.S, "Policy should be deterministic!"

        grid = self.get_map(env)
        fig, ax = self.render(grid, cbar=False, show=False)

        arrows = [(0, 1), (1, 0), (0, -1), (-1, 0)]
        scale = 0.5
        for i in range(env.w):
            for j in range(env.h):
                dx, dy = arrows[policy[env.coord_to_state(i, j)]]
                arrow = FancyArrow(i - scale/2 * dx, j - scale/2 * dy,
                                   scale * dx, scale * dy, color='black',
                                   length_includes_head=True, head_width=0.1)
                ax.add_patch(arrow)
        plt.show()

    def render_value_heatmap(self, evaluator, cmap='jet'):
        """Render the heatmap of the current value vector in evaluator."""
        self.render(self.get_value_grid(evaluator), cmap=cmap)

    def get_value_grid(self, evaluator):
        """Return the formatted value vector."""
        return self.vector_to_grid(evaluator.get_value(), evaluator.m)

    def get_value_grid_for_plot(self, evaluator, mask_walls=False):
        """Format the value vector for grid rendering."""
        grid = self.format_grid_for_plot(self.get_value_grid(evaluator))

        if mask_walls:
            mask = self.format_grid_for_plot(evaluator.m.map)
            return np.ma.masked_where(mask == 0, grid)
        return grid

    def vector_to_grid(self, v, env):
        """Transform vector into a grid corresponding to the GridWorld env."""
        return v.reshape((env.w, env.h), order='F')

    def render_value_propagation(self, evaluator, freq=1, interval=1.,
                                 limits=(0, 5), cmap='jet', nb_steps=np.inf,
                                 show=True):
        """
        Generates an animation of the value propagation in the GridWolrd grid
        through the evaluation process.

        limits defines the upper and lower value of the colorbar (can be None).

        Parameters:
        ----------
        evaluator : Evaluator object
            Iterative algorithm to compute the value vector of a policy in a
            GridWorld. Should have a step method.
        freq : int, default 1
            Number of iterations to execute before updating the plot.
        interval : float, default 1.
            Time between two consecutive updates, in milliseconds.
        limits : tuple of floats, default (0, 5)
            Lower and upper limits of the colorbar.
        cmap : string, default 'jet'
            The matplotlib colormap to use.
        nb_steps : int, default np.inf
            Number of iterations to show before stopping.
        show : boolean, default True
            Whether to start the animation or return the animation object.
        """
        cmap = plt.get_cmap(cmap)
        cmap.set_bad(color='black')  # walls appear in black
        cmap.set_under('white')
        self._n = 0

        def data_generator():
            while not(evaluator.should_stop() or self._n >= nb_steps):
                evaluator.step(freq)
                self._n += freq
                yield self.get_value_grid_for_plot(evaluator, mask_walls=True)

        fig, ax = plt.subplots()

        first_grid = self.get_value_grid_for_plot(evaluator, mask_walls=True)
        self._mat = ax.matshow(first_grid, cmap=cmap, origin='lower')
        self._mat.set_clim(limits)
        plt.colorbar(self._mat)

        ani = animation.FuncAnimation(fig, self._update_animation,
                                      data_generator,
                                      interval=interval)
        if show:
            plt.show()
        else:
            return ani

    def _update_animation(self, data):
        self._mat.set_data(data)
        return self._mat

    def render_value_propagation_FW(self, evaluator, interval=1., limits=(0, 5),
                                    cmap='jet', nb_steps=np.inf, show=True):
        """
        Generates an animation of the value propagation in the GridWolrd grid
        through the Floyd-Warshall evaluation process, with the integrated,
        border and next-to-be-integrated states.

        limits defines the upper and lower value of the colorbar (can be None).

        Parameters:
        ----------
        evaluator : Evaluator object
            Iterative algorithm to compute the value vector of a policy in a
            GridWorld. Should have a step method.
        freq : int, default 1
            Number of iterations to execute before updating the plot.
        interval : float, default 1.
            Time between two consecutive updates, in milliseconds.
        limits : tuple of floats, default (0, 5)
            Lower and upper limits of the colorbar.
        cmap : string, default 'jet'
            The matplotlib colormap to use.
        nb_steps : int, default np.inf
            Number of iterations to show before stopping.
        show : boolean, default True
            Whether to start the animation or return the animation object.
        """
        assert isinstance(evaluator, FWEvaluator)
        # assert evaluator.strat == 'extend hull'
        self._e = evaluator

        cmap = plt.get_cmap(cmap)
        cmap.set_bad(color='black')  # walls appear in black
        cmap.set_under('white')
        self._n = 0

        def data_generator():
            while not(evaluator.should_stop() or self._n >= nb_steps):
                evaluator.integrate(self._next_s)
                self._n += 1

                if evaluator.nb_of_states_left() > 0:
                    self._next_s = evaluator._get_next_state()

                yield self.get_value_grid_for_plot(evaluator, mask_walls=True)

        s_0 = evaluator._get_next_state()
        self._next_s = s_0

        fig, ax = plt.subplots()
        self._ax = ax

        first_grid = self.get_value_grid_for_plot(evaluator, mask_walls=True)
        self._mat = self._ax.matshow(first_grid, cmap=cmap, origin='lower')
        self._mat.set_clim(limits)
        plt.colorbar(self._mat)

        self._circle = self._draw_circle_FW()
        ax.add_patch(self._circle)

        self._border = plt.scatter(0, 0, c='black')

        ani = animation.FuncAnimation(fig, self._update_animation_FW,
                                      data_generator,
                                      interval=interval)
        if show:
            plt.show()
        else:
            return ani

    def _draw_circle_FW(self):
        s2c = self._e.m.state_to_coord
        return Circle(s2c(self._next_s), 0.3, color='white', fill=False)

    def _draw_border(self):
        e = self._e
        border = e.get_border()
        coord_x, coord_y = [], []
        for s in border:
            i, j = e.m.state_to_coord(s)
            coord_x.append(i)
            coord_y.append(j)
        return self._ax.scatter(coord_x, coord_y, c='white')

    def _update_animation_FW(self, data):
        previous = self._circle.get_center()
        self._circle.remove()
        self._ax.scatter(*previous, marker='x', c='white')
        self._circle = self._draw_circle_FW()
        self._border.remove()
        self._border = self._draw_border()
        self._ax.add_patch(self._circle)
        self._mat.set_data(data)
        return self._mat
