"""
Provide environments for testing.
"""
from itertools import product
import numpy as np
from MDP import MDP, GridWorld


def GW_three_blocks(scale=3, p_slipping=0.3):
    """Generate a gridworld with three blocks in the way, scaled by scale."""
    walls = ([(0+i, 1*scale+j) for i, j in product(range(scale), range(scale))]
             + [(2*scale+i, 0*scale+j) for i, j in product(range(scale),
                                                           range(scale))]
             + [(2*scale+i, 2*scale+j) for i, j in product(range(scale),
                                                           range(scale))])
    return GridWorld(3*scale, 4*scale, walls=walls,
                     goals=[((2*scale, 3*scale), 1.)], p_slipping=p_slipping)


def GW_random_lines(x, y, density=0.2):
    """Generate a random gridworld with line walls."""
    area, avg = x * y, (x + y)/2
    stop_proba = 1/(avg * density)  # 1/average length of lines of walls
    nb_walls = int(area * density)
    walls = np.zeros((x, y))
    directions = [(1, 1), (-1, 1), (-1, -1), (1, -1)]

    trials = 0
    while np.count_nonzero(walls) < nb_walls and trials < 5:
        # Find a new starting point without walls nearby
        i, j = np.random.randint(1, x-1), np.random.randint(1, y-1)

        if walls[i-1:i+1, j-1:j+1].sum() == 0:
            walls[i, j] = 1
            trials = 0
            direction = directions[np.random.randint(4)]

            while np.random.random() > stop_proba:
                if np.random.random() > 0.5:  # move along the x axis
                    i += direction[0]
                else:  # move along y axis
                    j += direction[1]

                if i == 0 or j == y-1:
                    break
                if i == x-1 or j == 0:
                    walls[i, j] = 1
                    break
                if walls[i-1:i+1, j-1:j+1].sum() > 2:
                    break
                walls[i, j] = 1

        else:
            trials += 1

    walls_list = mask_to_coords(walls)

    return GridWorld(x, y, walls=walls_list, start=(0, 0),
                     goals=[((x-1, y-1), 1.)], p_slipping=0.3)


def GW_random_maze(x, y, random_rewards=False, loop=False, absorbing=False):
    """
    Generate a random maze.
    Credits to jostbr
    https://github.com/jostbr/pymaze/blob/master/src/algorithm.py
    """
    assert not(absorbing) or not(loop), "can't have loop AND absorbing!"

    if x % 2 == 0:
        x += 1
    if y % 2 == 0:
        y += 1
    area = x * y

    walls = np.ones((x, y))
    visited = np.zeros((x, y), dtype=bool)

    # Coordinates and rewards of target tiles
    t1_x, t1_y, r1 = 0, y-1, 1.0
    t2_x, t2_y, r2 = x-1, 0, 1.0
    t3_x, t3_y, r3 = x-1, y-1, 1.0
    s_0 = (0, 0)

    goals = [((t1_x, t1_y), r1), ((t2_x, t2_y), r2), ((t3_x, t3_y), r3)]
    p_goal, mean_r = 0.2, 1.0
    if random_rewards:
        goals = []

    l = 2  # segment length
    path = []

    def get_unvisited_neighbours(i, j):
        n = list()

        if i > l-1:
            n.append((i-l, j))
        if i < x-l:
            n.append((i+l, j))
        if j > l-1:
            n.append((i, j-l))
        if j < y-l:
            n.append((i, j+l))

        return [neigh for neigh in n if not visited[neigh]]

    def cut_walls(i, j, a, b):
        walls[i, j] = 0
        walls[int((i+a)/2), int((j+b)/2)] = 0
        walls[a, b] = 0

    def draw_path(start):
        i, j = start
        nb_visited = 1
        visited[s_0] = True
        while nb_visited < area/2:
            n = get_unvisited_neighbours(i, j)
            if n:
                path.append((i, j))
                i_next, j_next = n[np.random.randint(0, len(n))]
                cut_walls(i, j, i_next, j_next)

                if random_rewards and np.random.random() < p_goal:
                    goals.append(((i_next, j_next),
                                  np.abs(np.random.normal(mean_r, 0.2))))
                
                visited[i_next, j_next] = True
                nb_visited += 1
                i, j = i_next, j_next

            elif path:
                i, j = path.pop()

            else:
                break

    draw_path(s_0)
    # visited = np.zeros((x, y), dtype=bool)
    # path = []
    # draw_path((x-1, y-1))
    # walls[1:(x-1):2, 1:(y-1):2] = 0

    while walls.sum() > area/3:
        if np.random.random() > 0.5:
            i = np.random.randint(1, x-5)
            j = np.random.randint(1, y-2)
            walls[i:i+4, j] = 0
        else:
            j = np.random.randint(1, y-5)
            i = np.random.randint(1, x-2)
            walls[i, j:j+4] = 0

    walls_list = mask_to_coords(walls)

    m = GridWorld(x, y, walls=walls_list, start=s_0, goals=goals,
                  p_slipping=0.3, loop=loop)

    if not absorbing:
        return m

    transi_to_sink = np.zeros((m.A, m.S + 1))
    transi_to_sink[:, -1] = 1.
    new_P = np.zeros((m.S+1, m.A, m.S+1))
    new_P[:m.S, :, :m.S] = m.P
    new_R = np.zeros((m.S+1, m.A))
    new_R[:m.S, :] = m.R
    for (i, j), _ in goals:
        index = m.coord_to_state(i, j)
        new_P[index, :, :] = transi_to_sink
    new_P[-1, :, -1] = 1.
    m.P, m.R = new_P, new_R
    m.S += 1
    m.states = np.arange(m.S)
    return m


def random_MDP(n, a, density=0.7, p_reward=0.01):
    """Randomly generate a MDP with n states and a actions."""
    lambda_ = max(1, int(n * density))
    P = np.zeros((n, a, n))

    for i in range(n):
        for j in range(a):
            nb_edges = min(np.random.poisson(lam=lambda_), n)
            successors = np.random.choice(n, size=nb_edges, replace=False)
            probas = np.random.random(size=nb_edges)
            P[i, j, successors] = probas/probas.sum()

    R = np.random.choice([1., 0.], size=(n, a), p=[p_reward, 1-p_reward])
    if np.count_nonzero(R) == 0:  # guarantee at least one reward
        R[np.random.choice(n), np.random.choice(a)] = 1.0
    return MDP(P, R, s_0=np.random.randint(n))


def mask_to_coords(mask):
    """Convert a 2D array of 0s and 1s into a list of coordinates of the 1s."""
    coords_list = list()
    coords_x, coords_y = mask.nonzero()
    for i, coord_x in enumerate(coords_x):
        coords_list.append((coord_x, coords_y[i]))

    return coords_list
