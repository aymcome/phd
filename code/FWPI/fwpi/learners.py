"""
Derive an optimal or near-optimal policy.
"""
import numpy as np
from matrix import Matrix, Tensor
from utility import get_maximising_policy, get_maximising_action
from utility import bellman_policy, bellman_optimal
from utility import policy_kernels
from policy_evaluation import linear_solving, Evaluator, LSEvaluator
from MDP import GridWorld


class Agent():
    """
    A Learning Agent to derive an (near-)optimal strategy.


    Parameters:
    ----------
    mdp : MDP object, with number of states and actions S and A
    gamma : 0 < float < 1, default 0.99
        Discount factor.
    """
    def __init__(self, mdp, gamma=0.99):
        self.m = mdp
        self.S = mdp.S
        self.A = mdp.A

        self.gamma = gamma

    def reset(self):
        self.m.reset()

    def play(self, s):
        """Play according to the current policy."""
        a = np.random.randint(self.A)
        self.m.step(a)
        return a

    def evaluate(self):
        """Return the resulting value vector."""
        return np.random.random(self.S)

    def plan(self):
        """Return the resulting policy."""
        return np.random.randint(self.A, size=self.S)


class VIAgent(Agent):
    """
    Value Iteration strategy to derive a near-optimal policy. Naive version,
    update values with every state.

    Parameters:
    ----------
    mdp : MDP object, with number of states and actions S and A
    v_0 : numpy.array with size S
        Initial value vector.
    epsilon : 0 < float, default 0.01.
        Approximation error for stopping.
    gamma : 0 < float < 1, default 0.99
        Discount factor.
    max_iter : int, default numpy.inf
        Maximum number of iterations to execute
    """
    def __init__(self, mdp, v_0, epsilon=0.01, gamma=0.99, max_iter=np.inf):
        super().__init__(mdp, gamma)

        assert v_0.size == mdp.S, f"Init vector should be of size {mdp.S}"
        self.v_0 = v_0

        self.eps = epsilon
        self.max_iter = max_iter
        self.thres = epsilon * (1-gamma)/gamma  # threshold for epsilon error

        self.n = 0  # current number of iterations done
        self.norm = np.linalg.norm

        # Value vectors used in the value iteration
        self.v_1, self.v_2 = v_0.copy() + 2 * self.thres, v_0.copy()
        # Array and Matrix used in the value iteration
        self._R = mdp.R.transpose((1, 0))
        self._P = Tensor(mdp.P.transpose((1, 0, 2)))

    def reset(self):
        self.m.reset()
        self.n = 0
        v_0 = self.v_0
        self.v_1, self.v_2 = v_0.copy() + 2 * self.thres, v_0.copy()

    def play(self, s):
        a = get_maximising_action(s, self.v_2, self.m, gamma=self.gamma)
        self.m.step(a)
        return a

    def step(self, n_iter=1):
        """Perform n_iter value iterations. Doesn't go over max_iter."""
        n = self.n
        max_iter = self.max_iter

        if n >= max_iter:
            print(f"Maximum number of cumulated iterations {max_iter} reached")
            return None

        n_iter = min(n + n_iter, max_iter)
        norm = self.norm
        thres = self.thres
        v_1, v_2 = self.v_1, self.v_2

        while (n < n_iter) and (norm(v_2 - v_1, np.inf) >= thres):
            v_1 = v_2
            v_2 = self.bellman_optimal(v_1)
            n += 1

        self.v_1, self.v_2 = v_1, v_2
        self.n = n

    def bellman_optimal(self, v):
        """Apply the optimal Bellman operator on the value vector v."""
        return np.max(self._R + self.gamma *
                      (self._P.vector_product(v)), axis=0)

    def evaluate(self):
        """Execute value iteration until threshold or max_iter are met."""
        self.step(n_iter=self.max_iter)
        return self.get_value()

    def plan(self):
        """Return the resulting policy."""
        return get_maximising_policy(self.evaluate(), self.m, gamma=self.gamma)

    def should_stop(self):
        """Return a boolean indicating if the evaluation is completed."""
        return ((self.n >= self.max_iter) |
                (self.norm(self.v_2 - self.v_1, np.inf) < self.thres))

    def get_value(self):
        """Return the current value vector."""
        return self.v_2


class VIAgentOpti(VIAgent):
    """
    Value Iteration strategy to derive a near-optimal policy. Look only at
    succesors for value update, not all the states.
    If the MDP has a 'get_neighbour_states' method, use it to get the
    neighbours of a state; otherwise compute them from the probability kernel.

    Parameters:
    ----------
    mdp : MDP object, with number of states and actions S and A
    v_0 : numpy.array with size S
        Initial value vector.
    epsilon : 0 < float, default 0.01.
        Approximation error for stopping.
    gamma : 0 < float < 1, default 0.99
        Discount factor.
    max_iter : int, default numpy.inf
        Maximum number of iterations to execute
    """
    def __init__(self, mdp, v_0, epsilon=0.01, gamma=0.99, max_iter=np.inf):
        if hasattr(mdp, "get_neighbour_states"):
            self.get_neighbour_states = mdp.get_neighbour_states

        super().__init__(mdp, v_0, epsilon=epsilon,
                         gamma=gamma, max_iter=max_iter)
        del self._R
        del self._P
        self.R, self.P = mdp.R, mdp.P

    def get_neighbour_states(self, s):
        """Return the list of succesors of s."""
        return np.nonzero(self.P[s, :])[0]

    def bellman_optimal(self, v):
        """Bellman operator using only the 4 neighbours of each state."""
        R, P, gamma = self.R, self.P, self.gamma
        new_v = R.copy()
        for s in range(self.S):
            neigh = self.get_neighbour_states(s)
            for a in range(self.A):
                for n in neigh:
                    new_v[s, a] += gamma * P[s, a, n] * v[n]
        return new_v.max(axis=1)


class PIAgent(Agent):
    """
    Policy Iteration strategy to derive an optimal policy.

    Parameters:
    ----------
    mdp : MDP object, with number of states and actions S and A
    pi_0 : numpy.array with size S
        Initial policy.
    evaluator : {None, Evaluator object}, default None
        The policy evaluator object to use for the Policy Evaluation step. If
        None, use Linear Solver.
    gamma : 0 < float < 1, default 0.99
        Discount factor.
    max_iter : int, default numpy.inf
        Maximum number of iterations to execute
    """
    def __init__(self, mdp, pi_0, evaluator=None, gamma=0.99, max_iter=np.inf):
        super().__init__(mdp, gamma)

        assert pi_0.shape == (mdp.S,), f"Init policy shape should be {mdp.S}"
        self.pi_0 = pi_0

        self.max_iter = max_iter
        self.n = 0  # iteration counter

        if evaluator is None:
            self.e = LSEvaluator(mdp, pi_0, gamma=gamma)
        elif isinstance(evaluator, Evaluator):
            self.e = evaluator
        else:
            raise TypeError("evaluator should be None or an Evaluator")

        # Value holders in the policy iteration
        self.pi_1, self.pi_2 = - np.ones(self.S), pi_0.copy()
        self.v = np.zeros(mdp.S)

    def reset(self):
        self.m.reset()
        self.n = 0

        pi_0 = self.pi_0
        self.e.set_policy(pi_0)
        self.pi_1, self.pi_2 = - np.ones(self.S), pi_0.copy()
        self.v = np.zeros(self.S)

    def play(self, s):
        a = self.pi_2[s]
        self.m.step(a)
        return a

    def step(self, n_iter=1):
        """Perform n_iter iterations of Policy Evaluation and Improvement."""
        n = self.n
        max_iter = self.max_iter

        if n >= max_iter:
            print(f"Maximum number of cumulated iterations {max_iter} reached")
            return None

        n_iter = min(n + n_iter, max_iter)

        pi_1, pi_2 = self.pi_1, self.pi_2
        evaluator = self.e  # already set to policy pi_2
        v = self.v

        while (n < n_iter) and not np.array_equal(pi_1, pi_2):
            pi_1 = pi_2
            v = evaluator.evaluate()
            pi_2 = get_maximising_policy(v, self.m, gamma=self.gamma)
            evaluator.set_policy(pi_2)
            n += 1

        self.pi_1, self.pi_2 = pi_1, pi_2
        self.v = v
        self.n = n

    def plan(self):
        """Execute policy iteration and return the resulting policy."""
        self.step(n_iter=self.max_iter)
        return self.get_policy()

    def evaluate(self, update=False):
        """
        Return the last value computed.
        If update is True, compute it for the current policy, instead of
        returning the last one used in the policy iteration.
        """
        if update:
            self.v = self.e.evaluate()
            self.e.reset()
            return self.v

        return self.v

    def should_stop(self):
        """Whether the stopping crition has been met or not."""
        return ((self.n >= self.max_iter) |
                np.array_equal(self.pi_1, self.pi_2))

    def get_policy(self):
        return self.pi_2


class MPIAgent(Agent):
    """
    Modified Policy Iteration strategy to derive a  near-optimal policy.

    Parameters:
    ----------
    mdp : MDP object, with number of states and actions S and A
    v_0 : numpy.array with size S
        Initial value vector.
    m : int, default 10
        The number of applications of the Bellman operator for policy
        evaluation.
    epsilon : 0 < float, default 0.01
        Approximation error for stopping criterion.
    gamma : 0 < float < 1, default 0.99
        Discount factor.
    max_iter : int, default numpy.inf
        Maximum number of iterations to execute
    """
    def __init__(self, mdp, v_0, m=10, epsilon=0.01, gamma=0.99,
                 max_iter=np.inf):
        super().__init__(mdp, gamma)

        assert v_0.shape == (mdp.S,), f"v_0 shape should be {mdp.S}"
        self.v_0 = v_0

        self.max_iter = max_iter
        self.n = 0  # iteration counter

        self.eps = epsilon
        self.k = m  # no overwrite of self.m
        self.norm = np.linalg.norm
        self.thres = epsilon * (1 - gamma) / gamma
        # Value holders in the policy iteration
        self.v_1, self.v_2 = v_0.copy() + 2 * self.thres, v_0.copy()
        self.pi = get_maximising_policy(v_0, self.m, gamma=self.gamma)

        self.update_policy_kernels()

    def reset(self):
        self.m.reset()
        self.n = 0

        self.v_1 = self.v_0.copy() + 2 * self.thres
        self.v_2 = self.v_0.copy()
        self.pi = get_maximising_policy(self.v_0, self.m, gamma=self.gamma)
        self.update_policy_kernels()

    def play(self, s):
        a = self.pi[s]
        self.m.step(a)
        return a

    def update_policy_kernels(self):
        """
        Update the reward array and the Matrix probability kernel to the
        current policy and mdp.
        """
        P, R = policy_kernels(self.m, self.pi)
        self.P, self.R = Matrix(P), R

    def step(self, n_iter=1):
        """Perform n_iter iterations."""
        n = self.n
        max_iter = self.max_iter

        if n >= max_iter:
            print(f"Maximum number of cumulated iterations {max_iter} reached")
            return None

        n_iter = min(n + n_iter, max_iter)

        v_1, v_2 = self.v_1, self.v_2
        norm, thres = self.norm, self.thres

        while (n < n_iter) and (norm(v_2 - v_1, np.inf) >= thres):
            v_1 = v_2
            self.pi = get_maximising_policy(v_2, self.m, gamma=self.gamma)
            self.update_policy_kernels()
            v_2 = self.policy_evaluation(v_2)
            n += 1

        self.v_1, self.v_2 = v_1, v_2
        self.n = n

    def policy_evaluation(self, v_0):
        """
        Execute the policy evaluation part by applying self.k times the Bellman
        operator on initial value vector v_0.
        """
        for _ in range(self.k):
            v_0 = self.R + self.gamma * (self.P.vector_product(v_0))
        return v_0

    def plan(self):
        """Execute policy iteration and return the resulting policy."""
        self.step(n_iter=self.max_iter)
        return self.get_policy()

    def evaluate(self, update=False):
        """
        Return the last value computed.
        If update is True, compute it for the current policy, instead of
        returning the last one used in the policy iteration.
        """
        if update:
            return self.policy_evaluation()

        return self.v_2

    def should_stop(self):
        """Whether the stopping criterion has been met."""
        return ((self.n >= self.max_iter) |
                (self.norm(self.v_2 - self.v_1, np.inf) < self.thres))

    def get_policy(self):
        return self.pi


# Functional approach


def value_iteration(mdp, v_0, epsilon=0.01, gamma=0.99, n_iter=np.inf):
    """
    Iterate the optimal Bellman Operator until the error threshold is met.

    Returns the near-optimal value vector.

    Parameters:
    ----------
    mdp : MDP instance
        MDP to be evaluated.
    v_0 : np.array of size mdp.S
        Initial value vector to iterate from.
    epsilon : float, default 0.01
        Error threshold for the infinity norm.
    gamma : 0. < float < 1., default 0.99
        Discount factor.
    n_iter : {np.inf, integer}, default np.inf
        Maximum number of iterations to perform, possibly infinite.
    """
    norm = np.linalg.norm
    thres = epsilon * (1 - gamma) / gamma  # threshold to get epsilon error

    v_1, v_2 = v_0.copy() + 2 * thres, v_0.copy()

    n = 0
    while (n < n_iter) and (norm(v_2 - v_1, np.inf) >= thres):
        v_1 = v_2
        v_2 = bellman_optimal(v_1, mdp, gamma=gamma, m=1)
        n += 1

    return v_2


def policy_iteration(mdp, policy_0, eval_method=None, gamma=0.99,
                     max_iter=np.inf):
    """
    Perform Policy Iteration on the MDP, starting from policy_0.

    Returns an (near-)optimal policy and its value vector.

    Parameters:
    ----------
    mdp : MDP instance
        MDP to be evaluated.
    policy_0 : array-like of size mdp.S
        Deterministic, stationary policy to be evaluated. policy[s] yields the
        action to be taken from state s.
    eval_method : function or None, default None
        eval_method(mdp, policy, gamma=gamma) should return the value vector of
        policy on mdp (possibly approximated). If None, use linear_solving from
        policy_evaluation.py.
    gamma : 0. < float < 1., default 0.99
        Discount factor.
    max_iter : {np.inf, integer}, default np.inf
        Maximum number of iterations to perform, possible infinite.
    """
    if not eval_method:
        eval_method = linear_solving

    pi_1, pi_2 = policy_0.copy(), policy_0.copy()

    v = eval_method(mdp, pi_2, gamma=gamma)
    pi_2 = get_maximising_policy(v, mdp, gamma=gamma)

    n = 0
    while (n < max_iter) and np.array_equal(pi_1, pi_2):
        pi_1 = pi_2
        v = eval_method(mdp, pi_2, gamma=gamma)
        pi_2 = get_maximising_policy(v, mdp, gamma=gamma)
        n += 1

    return pi_2, v


def modified_policy_iteration(mdp, v_0, m_n=10, epsilon=0.01, gamma=0.99,
                              max_iter=np.inf):
    """
    Perform Modified Policy Iteration on the MDP, starting from v_0.

    Returns an (near-)optimal policy and its approximate value vector.

    Parameters:
    ----------
    mdp : MDP instance
        MDP to be evaluated.
    v_0 : np.array of size mdp.S
        Initial value vector to iterate from.
    m_n : function or int, default 10
        Order sequence, indicates the number of applications of the Bellman
        operator during the policy evaluation step. If a function, m_n(n)
        returns that number for iteration number n.
    epsilon : float, default 0.01
        Error threshold for the infinity norm.
    gamma : 0. < float < 1., default 0.99
        Discount factor.
    max_iter : {np.inf, integer}, default np.inf
        Maximum number of iterations to perform, possibly infinite.
    """
    norm = np.linalg.norm
    thres = epsilon * (1 - gamma) / gamma  # threshold to get epsilon error

    if isinstance(m_n, int):
        m = lambda n: m_n
    else:
        m = m_n

    v_1, v_2 = v_0.copy() + 2 * thres, v_0.copy()

    n = 0
    while (n < max_iter) and (norm(v_2 - v_1, np.inf) >= thres):
        v_1 = v_2
        pi = get_maximising_policy(v_2, mdp, gamma=gamma)
        v_2 = bellman_policy(v_2, mdp, pi, gamma=gamma, m=m(n))

    return get_maximising_policy(v_2, mdp, gamma=gamma), v_2
