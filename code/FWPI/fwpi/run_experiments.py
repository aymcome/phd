"""
Run experiments for multiple algorithms on multiple environments.
"""
from experiments import PerformanceExpe


SIZES = [(5, 5), (11, 11), (15, 15)]
MDPS = ['Gridworld', 'RiverSwim']  # 'RandomDense', 'RandomSparse']

expe = PerformanceExpe(sizes=SIZES, mdps=MDPS, n_iter=3, optimal_policy=True,
                       gamma=0.98)

res = expe.run()
