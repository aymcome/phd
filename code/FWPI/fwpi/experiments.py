"""
Provide utilities for experiments.
"""
from time import time
from itertools import product
import numpy as np
import matplotlib.pyplot as plt
from policy_evaluation import Evaluator, LSEvaluator
from policy_evaluation import FWEvaluatorOpti, VIEvaluatorOpti
from learners import MPIAgent
from utility import generate_random_policy
from MDP import RiverSwim
from environments import GW_random_maze, random_MDP


COLORS = ['b', 'r', 'g', 'k', 'c', 'y']
MARKERS = ['o', 'x', 's', '+', '^', 'v']


class PEEvaluation():
    """
    Compare and display the results of several policy evaluators on one MDP and
    policy.
    Runtime and number of operations can be plot for multiple evaluators or
    multiple states.

    Parameters:
    ----------
    evaluators : list of Evaluator objects
        Evaluators to execute for comparison. All evaluators should be on the
        same MDP and same policy.
    """
    def __init__(self, evaluators):
        mdp, policy = evaluators[0].m, evaluators[0].p
        self.S = mdp.S

        for e in evaluators:
            assert isinstance(e, Evaluator), "Not an Evaluator object!"
            assert e.m == mdp, "All evaluators should have the same MDP!"
            assert (e.p == policy).all(), "Not the same policy!"
            e.reset()

        self.e = evaluators
        self.results = {e: None for e in self.e}

    def set_colors(self, colors=['b', 'r', 'g', 'k', 'c', 'y']):
        """Set the colors to be used for plots."""
        COLORS = colors

    def set_markers(self, markers=['o', 'x', 's', '+', '^', 'v']):
        """Set the markers to be used for plots."""
        MARKERS = markers

    def add(self, evaluator):
        """Add one or multiple evaluators."""
        if isinstance(evaluator, list):
            self.e = list(set(self.e) | set(evaluator))
        else:
            self.e = list(set(self.e) | {evaluator})
        self.check_compatibility()

    def remove(self, evaluator):
        """Remove one or multiple evaluators."""
        if isinstance(evaluator, list):
            self.e = list(set(self.e) - set(evaluator))
        else:
            self.e.remove(evaluator)

    def check_compatibility(self):
        """Check that evaluators are applied on the same MDP and policy."""
        if not self.e:
            print("No evaluator input.")

        assert isinstance(self.e[0], Evaluator), "Not an Evaluator object"
        mdp, policy = self.e[0].m, self.e[0].p

        for e in self.e:
            assert isinstance(e, Evaluator), "Not an Evaluator object!"
            assert e.m == mdp, "All evaluators should have the same MDP!"
            assert (e.p == policy).all(), "All evaluators should have the same policy!"

    def run(self, evaluators=None):
        """Run the evaluators, and save the statistics."""
        if evaluators is None:
            evaluators = self.e
        for e in evaluators:
            if e not in self.results or not self.results[e]:
                if not hasattr(e, "step"):
                    self.evaluate(e)

                else:
                    if e.should_stop():
                        e.reset()

                    self.run_evaluator(e)

    def reset(self, evaluator):
        evaluator.reset()
        self.results[evaluator] = None

    def reset_all(self):
        for e in self.e:
            self.reset(e)

    def evaluate(self, evaluator):
        t = time()
        v = evaluator.evaluate()
        t, c = time() - t, evaluator.get_complexity()
        self.results[evaluator] = v[np.newaxis, :], [t], [c]

    def run_evaluator(self, evaluator):
        """Run one evaluator, save the corresponding statistics."""
        if evaluator in self.results and self.results[evaluator]:
            return None

        runtimes, t_complex = [], []
        values = []

        values.append(evaluator.get_value().copy())
        runtimes.append(0)
        t_complex.append(0)

        t, c = time(), 0
        while not evaluator.should_stop():
            evaluator.step()
            runtimes.append(time() - t)
            c += evaluator.get_step_complexity()
            t_complex.append(c)
            values.append(evaluator.get_value().copy())

        self.results[evaluator] = np.array(values), runtimes, t_complex

    def sample_results(self, evaluator, freq=10):
        """Keep one out of freq measurement. Keep the first and last ones."""
        values, times, opes = self.results[evaluator]
        n_iter = len(times)
        indices = [0] + [i for i in range(freq, n_iter-1)]
        indices.append(n_iter-1)
        self.results[evaluator] = (values[indices, :],
                                   [times[i] for i in indices],
                                   [opes[i] for i in indices])

    def tweak_plot(self, xlabel, ylabel, title=None):
        """Finish setting up the plot and show it."""
        plt.xlabel(xlabel)
        plt.ylabel(ylabel)
        if title:
            plt.title(title)
        plt.grid()
        plt.legend()
        plt.show()

    def plot_values_along_complexity(self, state=None, evaluators=None,
                                     title=None):
        """Plot the evolving values at state for evaluators."""
        if state is None:
            state = np.random.randint(self.S)
        if evaluators is None:
            evaluators = self.e

        self.run(evaluators=evaluators)
        for e, c, m in zip(evaluators, COLORS, MARKERS):
            plt.plot(self.results[e][2], self.results[e][0][:, state],
                     label=e.name, color=c, marker=m)

        self.tweak_plot("Number of operations",
                        f"Estimated value of state {state}", title=title)

    def plot_states_along_complexity(self, evaluator, states, labels=None,
                                     title=None):
        """Plot the evolving values at multiple states for one evaluator."""
        if not labels:
            labels = [f"state {i}" for i in range(1, len(states)+1)]

        self.run_evaluator(evaluator)
        t, v = self.results[evaluator][2], self.results[evaluator][0]
        for s, c, m, l in zip(states, COLORS, MARKERS, labels):
            plt.plot(t, v[:, s], label=l, color=c, marker=m)

        self.tweak_plot("Number of operations",
                        f"Estimated state values by {evaluator.name}",
                        title=title)

    def plot_values_along_time(self, state=None, evaluators=None,
                               title=None):
        """Plot the evolving values at state for evaluators."""
        if state is None:
            state = np.random.randint(self.S)
        if evaluators is None:
            evaluators = self.e

        self.run(evaluators=evaluators)
        for e, c, m in zip(evaluators, COLORS, MARKERS):
            plt.plot(self.results[e][1], self.results[e][0][:, state],
                     label=e.name, color=c, marker=m)

        self.tweak_plot("Runtime (in seconds)",
                        f"Estimated value of state {state}",
                        title=title)

    def plot_states_along_time(self, evaluator, states, labels=None,
                               title=None):
        """Plot the evolving values at multiple states for one evaluator."""
        if not labels:
            labels = [f"state {i}" for i in range(1, len(states)+1)]

        self.run_evaluator(evaluator)
        t, v = self.results[evaluator][1], self.results[evaluator][0]
        for s, c, m, l in zip(states, COLORS, MARKERS, labels):
            plt.plot(t, v[:, s], label=l, color=c, marker=m)

        self.tweak_plot("Runtime (in seconds)",
                        f"Estimated state values by {evaluator.name}",
                        title=title)

    def plot_complexity_along_iterations(self, evaluators=None, title=None):
        """Plot the number of operations per iteration."""
        if evaluators is None:
            evaluators = self.e

        self.run(evaluators=evaluators)
        for e, c, m in zip(evaluators, COLORS, MARKERS):
            nb_opes = self.results[e][2]
            per_iteration = [nb_opes[i+1] - nb_opes[i]
                             for i in range(len(nb_opes)-1)]
            plt.plot(per_iteration, label=e.name, color=c, marker=m)

        self.tweak_plot("Iteration", "Number of edges reckoned with",
                        title=title)

    def plot_complexity_along_time(self, evaluators=None, title=None):
        """Plot the number of operations done per time."""
        if evaluators is None:
            evaluators = self.e

        self.run(evaluators=evaluators)
        for e, c, m in zip(evaluators, COLORS, MARKERS):
            nb_opes = self.results[e][2]
            times = self.results[e][1]
            plt.plot(times, nb_opes, label=e.name, color=c, marker=m)

        self.tweak_plot("Runtime", "Number of operations", title=title)


class PerformanceExpe():
    """
    Run experiments for several policy evaluators, on multiple MDPs, for
    different types and sizes.
    At the moment, GridWorld, RiverSwim, RandomDense and RandomSparse
    environments can be used, and FW, VI5%, VI1%, VI0.1% and LS are tested.

    Parameters:
    ----------
    sizes : list of (int, int), default [(11, 11), (25, 25), (51, 51)]
        GridWorld widths and heights to use for experiments. Use the same
        state space size for other environments.
    mdps : sublist of ['Gridworld', 'RiverSwim', 'RandomDense', 'RandomSparse']
        Environments to use for the experiments.
    n_iter : 0 < int, default 10
        Number of runs of evaluators on every instance. The results are
        averaged.
    optimal_policy : boolean, default True
        Whether to compute and use an optimal policy or a random one.
    gamma : 0 < float < 1, default 0.98
        Discount factor.
    """
    def __init__(self, sizes=[(11, 11), (25, 25), (51, 51)],
                 mdps=['Gridworld', 'RiverSwim', 'RandomDense', 'RandomSparse'],
                 n_iter=10, optimal_policy=True, gamma=0.98):
        self.sizes = sizes
        self.n = n_iter
        self.optimal_policy = optimal_policy
        self.gamma = gamma
        self.mdps = mdps
        self.names = ['FW', 'VI5', 'VI1', 'VI01'] #, 'LS']
        self.res = {(m, size): {name: None}
                    for m, size, name in product(self.mdps, sizes, self.names)}

    def run(self, savefile='expes/results.txt'):
        """Run the experiments and measure the performances."""
        n = self.n
        for mdp in self.mdps:
            for size in self.sizes:
                print(f"Starting runs on {mdp} with size {size}")
                times = {name: {'t': [], 'c': []} for name in self.names}

                for i in range(n):
                    res = self.run_mdp(mdp, size)
                    for e in self.names:
                        times[e]['t'].append(res[e][0][-1])
                        times[e]['c'].append(res[e][1][-1])

                for e in self.names:
                    self.res[mdp, size][e] = (sum(times[e]['t'])/n,
                                              sum(times[e]['c'])/n)
                    if savefile:
                        file = open(savefile, 'a')
                        file.write(
                            f"{mdp}, {size}, {e} : {self.res[mdp, size][e]}\n")
                        file.close()

        return self.res

    def run_mdp(self, mdp, size):
        """Generate a MDP instance and run evaluators on it."""
        x, y = size[0], size[1]
        S = x * y

        if mdp == 'Gridworld':
            m = GW_random_maze(x, y, random_rewards=False, loop=True)
        elif mdp == 'RiverSwim':
            m = RiverSwim(S)
        elif mdp == 'RandomDense':
            m = random_MDP(S, 4, density=0.7, p_reward=0.02)
        elif mdp == 'RandomSparse':
            m = random_MDP(S, 4, density=0.01, p_reward=0.02)

        return self.run_evaluators(m)

    def run_evaluators(self, mdp):
        """Run evaluators on the MDP."""
        gamma = self.gamma
        if self.optimal_policy:
            p = MPIAgent(mdp, np.zeros(mdp.S), gamma=gamma).plan()
        else:
            p = generate_random_policy(mdp.S, mdp.A)

        fw_e = FWEvaluatorOpti(mdp, p, gamma=gamma, strategy='extend hull')

        comparator = PEEvaluation([fw_e])
        comparator.run()

        v = fw_e.get_value()[mdp.s0]

        vi5_e = VIEvaluatorOpti(mdp, p, np.zeros(mdp.S), epsilon=0.05 * v,
                                gamma=gamma)
        vi1_e = VIEvaluatorOpti(mdp, p, np.zeros(mdp.S), epsilon=0.01 * v,
                                gamma=gamma)
        vi01_e = VIEvaluatorOpti(mdp, p, np.zeros(mdp.S), epsilon=0.001 * v,
                                 gamma=gamma)
        # ls_e = LSEvaluator(mdp, p, gamma=gamma)

        comparator.add([vi5_e, vi1_e, vi01_e]) #, ls_e])

        comparator.run()

        r = comparator.results
        return {name: (r[e][1], r[e][2])
                for name, e in zip(self.names,
                                   [fw_e, vi5_e, vi1_e, vi01_e])} #, ls_e])}

    def print_results(self):
        r = self.res
        for mdp, size in product(self.mdps, self.sizes):
            print(f"{mdp} with size {size}", end=' : ')
            for name in self.names:
                times = r[(mdp, size)][name]
                print(f"{name} took {times[0]} sc/{times[1]} operations", end=', ')
            print()

    def plot_operations_along_time(self, evaluator):
        """Visualise the relation between number of operations and runtime."""
        r = self.res
        for mdp, c, m in zip(self.mdps, COLORS, MARKERS):
            nb_opes = []
            times = []
            for s in self.sizes:
                nb_opes.append(r[(mdp, s)][evaluator][1])
                times.append(r[(mdp, s)][evaluator][0])
            plt.plot(times, nb_opes, color=c, marker=m, label=mdp)

        plt.xlabel("Total runtime")
        plt.ylabel("Total number of operations")
        plt.title(f"Complexity of {evaluator} on multiple MDPs")
        plt.grid()
        plt.legend()
        plt.show()

    def plot_performance(self, mdp):
        """Visualise how the algorithms performed on the MDP."""
        r = self.res
        sizes = [x * y for x, y in self.sizes]
        for e, c, m in zip(self.names, COLORS, MARKERS):
            nb_opes = []
            for s in self.sizes:
                nb_opes.append(r[(mdp, s)][e][1])
            plt.plot(sizes, nb_opes, color=c, marker=m, label=e)

        plt.xlabel("Number of states")
        plt.ylabel("Number of operations")
        plt.title(f"Performance of policy evaluation algorithms in {mdp}")
        plt.grid()
        plt.legend()
        plt.show()
