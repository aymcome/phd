# PHD - Approximation Methods for the Soundness of Control Laws derived by Machine Learning
December 2022 - November 2025 (hopefully)

[Devine](https://devine.inria.fr/) team, inria Rennes -> Supervisors: Loïc HÉLOUET & Éric FABRE

My email: <aymeric.come@inria.fr>

## Description
Work repository for my PHD, including latex notes, paper writing, code and experiments, and what not.